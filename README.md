# 停车场系统小程序

#### 介绍
功能都能正常使用的小程序，商业级完整源代码

 **【成品效果展示】:** 

![输入图片说明](gh_421c73099590_258.jpg)

 **【交流QQ群，广告勿入，否则终身拉黑】:**

![输入图片说明](%E6%99%BA%E6%85%A7%E5%9F%8E%E5%B8%82%E7%B3%BB%E7%BB%9F%E4%BA%A4%E6%B5%81%E7%BE%A4%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

跟演示效果完全一样的代码，没有任何缺失。如果没有数据，请自己对接自己的服务端接口，完全开源，不含有任何后门和木马病毒等，请放心使用，感谢支持

【 **互动交流** 】：本代码完整且可正常编译运行，没有任何代码缺失，如果无法跑起来的同学请先补一下小程序开发的基础知识，业余爱好分享，希望能对各位提供一定的帮助，若需不懂的地方可以加我微信: **Dove981011512** ，小白和没开发经验的就不要加我了，教基础的东西太难了，再次跟您说声对不起
【业余兼职】：如果您需要我帮你做一些小程序界面设计和功能小定制的(仅限前端)，付点茶钱，也是可以的，单次不低于5000，因为我平时上班比较忙，还望您体谅

【开发建议】：想研究相机对接的，建议自己买个开发机器，连个两三百的开发机器你都没有，不要谈啥研究车牌识别系统。


#### 安装教程

1.  下载该代码到你自己的电脑上
2.  使用HBuilder编辑器打开项目，如果你电脑上没有该编辑工具请下载：https://www.dcloud.io/hbuilderx.html
3.  找到parking/pages/index/index/vue
4.  配置好的Hbuilder，例如设置好微信开发者工具的路径，小程序appid等
5.  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/111316_2969e02c_753802.png "微信图片_20200805111256.png")
6. ![输入图片说明](https://images.gitee.com/uploads/images/2020/0813/141856_3dae67fa_2176643.png "微信截图_20200805111922.png")
7. ![输入图片说明](https://images.gitee.com/uploads/images/2020/0813/141914_1301e7eb_2176643.png "微信图片_20200805111256.png")


#### 使用说明

1.  服务端(管理)使用教程：https://www.showdoc.com.cn/2192066741521376/10290721898432961
2.  后台管理设置好自己的相关微信账号
3.  直接使用
4.  服务端：https://gitee.com/wangdefu/caraprk_public

备注：服务端只是基础框架，需要完善的功能请个人自行完善接口开发，祝您工作愉快

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
